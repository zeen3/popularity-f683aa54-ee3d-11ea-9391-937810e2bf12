//! popularity over specific periods of time
use {
	derive_more::{Add, AsRef, Deref, DerefMut, Sum},
	rayon::prelude::*,
	std::{
		cell::Cell,
		collections::HashMap,
		fmt, mem,
		sync::{atomic::*, Arc, Mutex, RwLock},
		thread::yield_now,
	},
};
const SUMLEN: usize = 64;
pub trait IdxSumT: Copy + std::fmt::Display + Default {}
impl IdxSumT for u64 {}
impl IdxSumT for u32 {}
impl IdxSumT for u16 {}
#[derive(AsRef, Clone, Copy, DerefMut, Deref)]
pub struct IdxSum<T: IdxSumT = u64>([(u64, T); SUMLEN]);
impl<T: IdxSumT + 'static> fmt::Debug for IdxSum<T> {
	fn fmt(
		&self,
		f: &mut fmt::Formatter,
	) -> fmt::Result {
		f.debug_struct("IdxSum")
			.field("len", &SUMLEN)
			.field("items", &[..])
			.finish()
	}
}
impl<T: IdxSumT> Default for IdxSum<T> {
	fn default() -> Self {
		Self([Default::default(); SUMLEN])
	}
}
const LAST28: usize = 24 * 7 * 28;
#[derive(Clone)]
/// a struct holding an index for the last 28 days
struct Wrap28(Cell<usize>);
unsafe impl Send for Wrap28 {}
unsafe impl Sync for Wrap28 {}
impl Wrap28 {
	/// creates a nil item, starting from zero
	fn new() -> Self {
		Self(Cell::new(LAST28 - 1))
	}
	/// creates the next index, according to
	/// the internal state.
	unsafe fn next(&self) -> usize {
		let mut m = self.0.get();
		m += 1;
		let max = LAST28 * 2;
		if m == max {
            // sets next to 1
			self.0.set(LAST28);
			0
		} else {
            // sets next to 1+now
			self.0.set(m);
			m % LAST28
		}
	}
}
impl Default for Wrap28 {
	fn default() -> Self {
		Self::new()
	}
}
impl fmt::Debug for Wrap28 {
	fn fmt(
		&self,
		f: &mut fmt::Formatter,
	) -> fmt::Result {
		let idx = self.0.get() % LAST28;
		f.debug_struct("Wrap28").field("idx", &idx).finish()
	}
}
/// month, week, day, night cycles.
#[derive(Add, Clone, Copy, Default, Debug, Sum)]
struct MWDN(u64, u64, u64, u64);
impl Into<[u64; 4]> for MWDN {
	/// easy array conversions
	fn into(self) -> [u64; 4] {
		[self.0, self.1, self.2, self.3]
	}
}
impl<T: Into<u64>> From<[T; 4]> for MWDN {
	/// easy array conversions
	fn from([m, w, d, n]: [T; 4]) -> Self {
		Self(m.into(), w.into(), d.into(), n.into())
	}
}
/// large structure with internal mutability that needs send + sync
#[derive(Clone)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
struct Last28 {
	/// last 28 days in hourly forms
	// if we get over 4 billion hits in an hour we're fucked
	hrs: [u32; LAST28],
	#[cfg_attr(feature = "serde", serde(skip))]
	/// a summative cache, which is never (de)serialized by serde.
	sum: Cell<Option<MWDN>>,
}
unsafe impl Sync for Last28 {}
unsafe impl Send for Last28 {}
impl Default for Last28 {
	fn default() -> Self {
		Self {
			hrs: [0; LAST28],
			sum: Cell::new(None),
		}
	}
}
impl fmt::Debug for Last28 {
	fn fmt(
		&self,
		f: &mut fmt::Formatter,
	) -> fmt::Result {
		let cache = self.sum.get();
		// don't overload the formatter
		let hrs = [..];
		f.debug_struct("Last28")
			.field("hrs", &hrs)
			.field("mwdn_sum_cache", &cache)
			.finish()
	}
}
impl Last28 {
	/// gets the month-week-day-night time range sums
	///
	/// this is *technically* safe, but shouldn't be called
	/// in quick succession, and *especially* not on multiple threads.
	unsafe fn get_days_ranges(
		&self,
		idx: usize,
		mdiff: i64,
	) -> MWDN {
		const N: usize = 12;
		const D: usize = N * 2;
		const W: usize = D * 7;
		let mut l = self.sum.get();
		if let Some(mwdn) = l.as_mut() {
			let diff = |x, mwdnv: &mut u64| {
				let i = if idx <= x {
					idx - x
				} else {
					idx + (LAST28 - x)
				};
				let val = self.hrs[i] as u64;
				if *mwdnv >= val {
					*mwdnv -= *mwdnv - val;
				} else {
					*mwdnv += val - *mwdnv;
				}
			};
			diff(N, &mut mwdn.3);
			diff(D, &mut mwdn.2);
			diff(W, &mut mwdn.1);
			let m = &mut mwdn.0;
			if mdiff > 0 {
				*m += mdiff as u64;
			} else {
				*m -= mdiff.abs() as u64;
			}
			self.sum.set(Some(*mwdn));
			*mwdn
		} else {
			let sum = self
				.hrs
				.par_iter()
				.enumerate()
				.map(|(i, &m)| {
					let dist_back = if i <= idx {
						// we're behind or on it in the buffer
						idx - i
					} else {
						// we're ahead of it in the buffer, and wrap around
						idx + (LAST28 - i)
					};
					// codegen bs
					let v = |ok| if ok { m as _ } else { 0 };
					let n = v(dist_back < N);
					let d = v(dist_back < D);
					let w = v(dist_back < W);
					// month is always an identity sum
					MWDN(m as _, w, d, n)
				})
				.sum();
			self.sum.set(Some(sum));
			sum
		}
	}
}
/// sums of previous hours
#[derive(Clone, Copy, Default, Debug)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct PrevHrs {
	/// previous hour, by index
	last_hour: IdxSum<u32>,
	/// previous night (12 hr), by index
	last_12_hr: IdxSum,
	/// previous day (24 hr), by index
	last_day: IdxSum,
	/// previous week (168 hr or 7 days), by index
	last_week: IdxSum,
	/// previous day (672 hr or 28 days), by index
	last_month_ish: IdxSum,
}
macro_rules! iter {
    ($t:ty: $($i:ident: $v:ident),+) => {$(
        pub fn $i(&self) -> Vec<(u64, $t)> {
            let idx = self.$v.iter()
                .position(|&x| x.0 == 0)
                .unwrap_or(64);
            self.$v[..idx].to_vec()
        }
    )+}
}
impl PrevHrs {
	iter!(u32: hour: last_hour);
	iter!(
		u64: night: last_12_hr,
		day: last_day,
		week: last_week,
		month: last_month_ish
	);
	/// get relevant ids, for any related use.
	///
	/// this function makes no guarantees about the
	/// number of elements returned.
	pub fn get_ids(&self) -> Vec<u64> {
		let mut v = Vec::with_capacity(SUMLEN * 5);
		v.extend(self.last_hour.0.iter().filter_map(|i| {
			if i.0 == 0 {
				None
			} else {
				Some(i.0)
			}
		}));
		let mut fm = |i: IdxSum| {
			v.extend(
				i.iter()
					.filter_map(|x| if x.0 == 0 { None } else { Some(x.0) }),
			)
		};
		fm(self.last_12_hr);
		fm(self.last_day);
		fm(self.last_week);
		fm(self.last_month_ish);
		v.par_sort_unstable();
		v.dedup();
		v
	}
}
// internal type to simplify
type HrCnt = HashMap<u64, AtomicU32>;
/// main popularity structure
#[derive(Debug, Default, Clone)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct Popularity {
	/// Recent popularity history.
	///
	/// Each entry is obtrusively massive, on purpose.
	///
	/// With specialisation, this could be restructured to keep
	/// a rolling count instead of full, immutable state. This,
	/// however, is somewhat non-trivial for the implementation.
	history: Arc<Mutex<HashMap<u64, Last28>>>,
	/// current offset in the popularity history
	offset: Wrap28,
	// needs a better way of getting at it in a multithreaded way
	// but will figure that out later maybe. Currently counts
	// 100% of cached items locklessly, but locks the cache
	// if an id isn't already in there or when the inc_hr
	// function is called.
	/// current hour tracker
	#[cfg_attr(feature = "serde", serde(skip))]
	curr_hour: Arc<RwLock<HrCnt>>,
	/// previous hour cache (reuse allocation)
	#[cfg_attr(feature = "serde", serde(skip))]
	prev_hour: Arc<Mutex<HrCnt>>,
	/// amount of ids awaiting writes
	#[cfg_attr(feature = "serde", serde(skip))]
	waiting_write: Arc<AtomicUsize>,
}
pub struct IdCnt(u64, u32);
impl From<&u64> for IdCnt {
	fn from(u: &u64) -> Self {
		(*u).into()
	}
}
impl From<u64> for IdCnt {
	fn from(u: u64) -> Self {
		Self(u, 1)
	}
}
impl<T: Into<Option<u32>>> From<(u64, T)> for IdCnt {
	fn from(u: (u64, T)) -> Self {
		Self(u.0, u.1.into().unwrap_or(1))
	}
}
impl Popularity {
	fn wait_write(&self) {
		// avoid blocking a write, esp if it's probably a popular one
		// since it's an extreme amount slower to take that else branch
		let mut cnt = 64;
		while self.waiting_write.load(Ordering::SeqCst) != 0 {
			// busy
			cnt -= 1;
			if cnt == 0 {
				// max total loops, break and wait
				// on a blocking syscall
				yield_now();
				break;
			}
		}
	}
	/// creates a new popularity tracker
	pub fn new() -> Self {
		Self::default()
	}
	/// increment an id, by any amount (if None is provided,
	/// rolls to 1).
	pub fn inc_id<IDC: Into<IdCnt>>(
		&self,
		id_cnt: IDC,
	) {
		let IdCnt(id, by) = id_cnt.into();
		self.wait_write();
		if let Some(atom) = self.curr_hour.read().unwrap().get(&id) {
			// atomic allows &self += n through fetch_add
			atom.fetch_add(by, Ordering::SeqCst);
		} else {
			// push write holster
			self.waiting_write.fetch_add(1, Ordering::Acquire);
			// get through this asap we're blocking other lads
			*self
                .curr_hour
                .write()
                .unwrap()
                .entry(id)
                .or_default()
                // we have &mut, go fucking faster
                .get_mut() += by;
			// we may have other lads waiting
			// pop write holster
			self.waiting_write.fetch_sub(1, Ordering::Release);
		}
	}
	/// increments a batch of ids. can be useful for eventual consistency
	/// in counting views, but needs rework. Uses a single lock
	pub fn inc_ids_batch<IDC: Into<IdCnt>, I: IntoIterator<Item = IDC>>(
		&self,
		ids: I,
	) {
		let ids: Vec<IdCnt> = ids.into_iter().map(Into::into).collect();
		// wait for readable
		self.wait_write();
		let read_lock = self.curr_hour.read().unwrap();
		let unadded: Vec<_> = ids
			.into_par_iter()
			.filter_map(|idcnt| match read_lock.get(&idcnt.0) {
				Some(n) => {
					n.fetch_add(idcnt.1, Ordering::SeqCst);
					None
				}
				None => Some(idcnt),
			})
			.collect();
		// if we've got nothing to add, don't bother trying.
		if !unadded.is_empty() {
			// wait for writable, but block further readers
			self.waiting_write.fetch_add(1, Ordering::Acquire);
			// early drop to collect a write lock
			drop(read_lock);
			let mut write_lock = self.curr_hour.write().unwrap();
			for IdCnt(id, cnt) in unadded {
				*write_lock.entry(id).or_default().get_mut() += cnt;
			}
			self.waiting_write.fetch_sub(1, Ordering::Relaxed);
		}
	}
	pub fn inc_hrs(&self) -> PrevHrs {
		let mut prev_hr = self.prev_hour.lock().unwrap();
		self.wait_write();
		// reversed acquire order, to allow last minute
		// additions to the write buffer.
		self.waiting_write.fetch_add(1, Ordering::Relaxed);
		// this is probably bad but if we've panicked while
		// we have the mutex we're screwed anyway.
		let mut guard = self.curr_hour.write().unwrap();
		mem::swap(&mut *guard, &mut *prev_hr);
		drop(guard);
		self.waiting_write.fetch_sub(1, Ordering::Acquire);
		// SAFETY: We only ever fetch this value in this function,
		// and only when we have prev_hour locked.
		let off = unsafe { self.offset.next() };
		let mut pop_hr: Vec<(u64, u32)> = prev_hr
			.par_iter_mut()
			.map(|(id, cnt)| {
				let cnt = cnt.get_mut();
				let ocnt = *cnt;
				*cnt = 0;
				(*id, ocnt)
			})
			.collect();
		pop_hr.par_sort_unstable_by_key(|(_id, count)| *count);
		let mut last_hrs = PrevHrs::default();
		macro_rules! pop_time {
			($pop:expr, $hrs:expr) => {{
				let pop = $pop;
				let hrs = $hrs;
				pop.par_iter()
									                    .take(SUMLEN)
									                    .map(|&c| c)
									                    .chain(rayon::iter::repeatn(
									                        (0, 0),
									                        SUMLEN.saturating_sub(pop.len()),
									                    ))
									                    .zip_eq(hrs.par_iter_mut())
									                    // set last back to our thing
									                    .for_each(|(new, old)| {
									                        *old = new;
									                    });
				}};
		};
		pop_time!(&pop_hr, &mut last_hrs.last_hour[..]);
		#[derive(Add, Sum)]
		struct Avg(u64, u64);
		let Avg(sum, cnt) = pop_hr
			.par_iter()
			.filter_map(|&(_id, cnt)| {
				if cnt <= 1 {
					None
				} else {
					Some(Avg(cnt as u64, 1))
				}
			})
			.sum();
		let avg = (sum >> 1 / cnt) as u32;
		// prime next hrs cache for items above half the average
		// can't be rayonised
		for (id, _cnt) in pop_hr.iter().skip_while(|(_id, cnt)| *cnt > avg) {
			match prev_hr.remove(id).map(|v| v.into_inner()) {
				Some(0) => {}
				Some(_) => {
					panic!("we set these to zero tho");
				}
				None => {
					panic!("we haven't taken this away yet tho");
				}
			}
		}
		let mut history = self.history.lock().unwrap();
		// this is the big part i can't rayonize
		let pop_hr: Vec<(u64, i64)> = pop_hr
			.iter()
			.map(|(id, cnt)| {
				// so we get a history list
				let hist = history.entry(*id).or_default();
				// we do a diff for later
				let diff = hist.hrs[off] as i64 - *cnt as i64;
				// we set hrs to the right value
				hist.hrs[off] = *cnt;
				// and we hold onto it
				(*id, diff)
			})
			.collect();
		let v = || Vec::with_capacity(history.len() >> 4);
		let mut lmwdn: [Vec<(u64, u64)>; 4] = history
            .par_iter()
            // converts each history entry to a
            // (id, [month, week, day, noon-cycle])
            // tuple
            .map(|(id, hist)| {
                let diff = pop_hr
                    .iter()
                    .find(|(i, _)| i == id)
                    .map(|(_, diff)| *diff as i64)
                    .unwrap_or(0);
                // SAFETY: This is done on each internally
                // mutable range on a separate thread each.
                // It is specifically not done on cloned,
                // Arc<_>, Rc<_>, or other internally mutable
                // constructs.
                let ranges: [u64; 4] = unsafe { hist.get_days_ranges(off, diff) }.into();
                (id, ranges)
            })
            // .map(|(id, [lm, lw, ld, l12])| {
            //     // converts each of those to 4 single-element
            //     // Vec<(id, val)> arrays.
            //     let v = |l| vec![(*id, l)];
            //     [v(lm), v(lw), v(ld), v(l12)]
            // })
            .fold(
                || [v(), v(), v(), v()],
                |mut b, idl| {
                    let (&id, [lm, lw, ld, l12]) = idl;
                    b[0].push((id, lm));
                    b[1].push((id, lw));
                    b[2].push((id, ld));
                    b[3].push((id, l12));
                    b
                },
            )
            .reduce(
                || [v(), v(), v(), v()],
                |mut a, mut b| {
                    // with each of array A (4 items)
                    let () = a
                        .par_iter_mut()
                        // and of array B (4 items)
                        .zip_eq(b.par_iter_mut())
                        // append the contents of each B to A
                        .for_each(|(a, b)| a.append(b));
                    // and drop empty vec B
                    a
                },
            );
		drop(history);
		lmwdn
			.par_iter_mut()
			.for_each(|vec| vec.par_sort_unstable_by_key(|(_id, cnt)| *cnt));
		//let [mut lm, mut lw, mut ld, mut l12] = lmwdn;
		let [lm, lw, ld, l12] = lmwdn;
		pop_time!(&lm, &mut last_hrs.last_month_ish[..]);
		pop_time!(&lw, &mut last_hrs.last_week[..]);
		pop_time!(&ld, &mut last_hrs.last_day[..]);
		pop_time!(&l12, &mut last_hrs.last_12_hr[..]);
		last_hrs
	}
}
